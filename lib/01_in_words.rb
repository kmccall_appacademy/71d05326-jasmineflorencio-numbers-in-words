class Fixnum

  def one_to_ninety
    {0=>"zero",1=>"one",2=>"two",3=>"three",4=>"four",5=>"five",6=>"six",7=>"seven",8=>"eight",9=>"nine",
      10=>"ten",11=>"eleven",12=>"twelve",13=>"thirteen",14=>"fourteen",
      15=>"fifteen",16=>"sixteen",17=>"seventeen",18=>"eighteen",19=>"nineteen",
      20=>"twenty",30=>"thirty",40=>"forty",50=>"fifty",60=>"sixty",
      70=>"seventy", 80=>"eighty", 90=>"ninety"}
  end

  def in_words
    if self.to_s.length == 1
      one_to_ninety[self]
    else
      chunks = []
      number = self
      until number == 0
        chunks << hundreds(number % 1000)
        number = number / 1000
      end
      if chunks.length == 1
        chunks[0]
      elsif chunks.length == 2
        thousands(chunks)
      elsif chunks.length == 3
        millions(chunks)
      elsif chunks.length == 4
        billions(chunks)
      elsif chunks.length == 5
        trillions(chunks)
      else
        "Please enter a number between 0 - 999,999,999,999,999"
      end
    end
  end


  def tens(num)
    num = num % 100
    if one_to_ninety.keys.include?(num)
      one_to_ninety[num]
    else
      one_to_ninety[num.to_s[0].to_i * 10] + " " + one_to_ninety[num.to_s[1].to_i]
    end
  end

  def hundreds(num)
    if num.to_s.length == 1
      one_to_ninety[num]
    elsif num.to_s.length == 2
      tens(num)
    elsif num.to_s[-2..-1] == "00"
      one_to_ninety[num.to_s[0].to_i] + " hundred"
    else
      one_to_ninety[num.to_s[0].to_i] + " hundred " + tens(num.to_s[-3..-1].to_i)
    end
  end

  def thousands(array)
    if array[0] == "zero"
      array[1] + " thousand"
    elsif array[1] == "zero"
      array[0]
    else
      array[1] + " thousand " + array[0]
    end
  end

  def millions(array)
    if array[2] == "zero"
      thousands(array[0..1])
    elsif array[0] == "zero" && array[1] == "zero"
      array[2] + " million"
    elsif array[1] == "zero"
      array[2] + " million " + array[0]
    elsif array[0] == "zero"
      array[2] + " million " + array[1] + " thousand"
    else
      array[2] + " million " + array[1] + " thousand " + array[0]
    end
  end

  def billions(array)
    if array[3] == "zero"
      millions(array[0..2])
    elsif array[0] == "zero" && array[1] == "zero" && array[2] == "zero"
      array[3] + " billion"
    else
      array[3] + " billion " + millions(array[0..2])
    end
  end

  def trillions(array)
    if array[0] == "zero" && array[1] == "zero" && array[2] == "zero" && array[3] == "zero"
      array[4] + " trillion"
    else
      array[4] + " trillion " + billions(array[0..3])
    end
  end

end
